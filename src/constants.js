export const PROJECT_TYPE = [
    { label: 'Web app', value: 'web' },
    { label: 'Mobile app', value: 'mobile' },
    { label: 'Marketing', value: 'marketing' },
];

export const USER_ROLES = [
    { label: 'Developer', value: 'dev' },
    { label: 'Project Manager', value: 'pm' }
]

export const TASK_TYPE = [
    { value: 'bug', label: 'Bug' },
    { value: 'task', label: 'Task' },
    { value: 'enh', label: 'Enhancement' }
];

export const TASK_CATEGORY = [
    { value: 'fe', label: 'Frontend' },
    { value: 'be', label: 'Backend' },
    { value: 'pm', label: 'Project Management' },

];
