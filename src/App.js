import React, { useState, createContext } from 'react';
import Layout from '../src/containers/Layout/Layout';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import styles from './App.module.css';
import '../src/assets/tailwind-output.css';
import Routes from './router.js';
import { AuthProvider } from "./contexts/AuthContext";

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const AuthContext = createContext(null);

const App = () => {

	return (
		<>
			<AuthProvider>
				<div className={styles.LayoutWrapper}>
					<Router>
						<Layout>
							<Switch>
								<Routes />
							</Switch>
						</Layout>
					</Router>
				</div>

			</AuthProvider>
			<ToastContainer />
		</>
	);
};

export default App;
