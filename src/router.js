import React from 'react';
import { Route, useHistory , Switch, Redirect} from 'react-router-dom';
import Projects from './pages/Projects/Projects';
import ProjectDetails from './pages/ProjectDetails/ProjectDetails';
import ProjectDetailsEdit from './pages/ProjectDetailsEdit/ProjectDetailsEdit';
import Users from '../src/pages/Users/Users';
import Kanban from '../src/pages/Kanban/Kanban';
import '../src/assets/tailwind-output.css';
import Login from './pages/Login/Login.js';
import 'firebase/auth';
import {getUser} from './contexts/AuthContext';

export default function Router() {
	
	const user = getUser();

	const renderRoutes = () => {
		if (user) {
			return (
				<>
					<Route exact path={['/', '/projects']} component={Projects} />
					<Route exact path="/users" component={Users} />
					<Route exact path="/login" >
						<Redirect to="/projects" />
					</Route>
					<Route exact path="/project/:id" component={ProjectDetails} />
					<Route exact path="/project/:id/edit" component={ProjectDetailsEdit} />
					<Route exact path="/project/:id/kanban/" component={Kanban} />
				</>
			);
		}

		return (
			<>
				<Route exact path={["/","/login"]} component={Login}/>
			</>
		);
	};
	return <Switch>{renderRoutes()}</Switch>;
}




