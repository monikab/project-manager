import React, { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import NavigationItems from '../NavigationItems/NavigationItems';
import AuthContext from '../../../contexts/AuthContext';
import styles from './Toolbar.module.css';
import { getUser } from '../../../contexts/AuthContext';
import { ReactComponent as ProfileIcon } from '../../../assets/icons/user-icon.svg';
import { ReactComponent as SignOutIcon } from '../../../assets/icons/sign-out-icon.svg';

const Toolbar = () => {
	const [error, setError] = useState('');
	const { logout } = useContext(AuthContext);
	const history = useHistory();
	const user = getUser();
	const userRole = localStorage.getItem('userRole')

	async function handleLogout() {
		setError('');

		try {
			await logout();
			history.push('/login');
		} catch {
			setError('Failed to log out');
		}
	}
	return (
		<div className={`${styles.Toolbar} flex-col fixed`}>
			<div>

				<h1 className="text-xl uppercase mb-10 text-bold font-bold">
					Project manager application
			</h1>
				{user && (<>
					<h2 className={`${styles.Profile}`} ><ProfileIcon className="h-5 w-5 mr-2 mt-1" /><span>{user.email}<br></br><span className="text-sm">Role: {(userRole === 'pm' && 'Project manager')} {(userRole === 'dev' && 'Developer')} {(userRole === 'admin' && 'Admin')}</span> </span></h2>


					<NavigationItems />

					<a className="mt-auto cursor-pointer flex absolute bottom-6" onClick={handleLogout}>
						<SignOutIcon className="h-5 w-5 mr-2" />Logout
				</a>
				</>)
				}
			</div>
		</div>
	);
};

export default Toolbar;
