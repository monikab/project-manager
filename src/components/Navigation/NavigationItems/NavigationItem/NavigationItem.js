import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './NavigationItem.module.css';
const navigationItem = ({link, children}) => (
	<li>
		<NavLink to={link} activeClassName={styles.active}>
			{children}
		</NavLink>
	</li>
);

export default navigationItem;