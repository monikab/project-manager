import React from 'react';
import NavigationItem from './NavigationItem/NavigationItem';


const NavigationItems = () => (
	<ul>
		<NavigationItem link="/projects">Projects</NavigationItem>
		<NavigationItem link="/users">Users</NavigationItem>
	</ul>
);

export default NavigationItems;