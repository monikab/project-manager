import React, { useContext, useRef, useState, useEffect, useMemo } from "react"
import FieldInput from '../FieldInput/FieldInput.js';
import { useForm } from 'react-hook-form';
import Select from '../Select/Select.js';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useParams } from 'react-router-dom';
import { TASK_CATEGORY, TASK_TYPE } from '../../constants';
import { db } from '../../firebase.config.js';
import 'firebase/database';
import 'firebase/firestore'
import { toast } from "react-toastify";
const EditTaskSchema = Yup.object().shape({
    name: Yup.string().required('Required field'),
});


const EditTaskForm = ({ data, tasksList, handleEditData, handleEditModalToggle }) => {

    const { register, handleSubmit, watch, errors, control } = useForm({
        resolver: yupResolver(EditTaskSchema),
        mode: 'onChange',
    });

    const taskData = {
        id: data.id,
        name: data.name,
        description: data.description,
        type: data.type,
        category: data.category,
        assignee: data.assignee,
        status: data.status
    };

    let { id } = useParams();

    const [error, setError] = useState('');
    const [task, setTask] = useState(taskData);

    const [users, setUsers] = useState();

    const [selectTaskType, setSelectTaskType] = useState();
    const [selectTaskCategory, setSelectTaskCategory] = useState();
    const [selectTaskAssignee, setSelectTaskAssignee] = useState();

    const [currentProject, setCurrentProject] = useState();
    const [projectKey, setProjectKey] = useState();


    async function getUsers() {
        const loadedUsers = await db.collection("users")
            .get()
            .then(querySnapshot => {
                const data = querySnapshot.docs.map(doc => doc.data());
                return data

            });
        setUsers(loadedUsers)
    }



    let taskTypeOptions = [];
    let taskCategoryOptions = [];


    const getTaskTypes = (task_types) => {
        task_types.map(type => {
            const { name, value } = type
            taskTypeOptions.push(type)
        })
    }

    const getTaskCategory = (task_categories) => {
        task_categories.map(type => {
            const { name, value } = type
            taskCategoryOptions.push(type)
        })
    }

    const taskMembersOptions = useMemo(() => {
        if (users) {
            let developers = users && users.filter(el => el.role !== 'pm' && el.role !== 'admin')

            return developers.map(user => ({ value: user.id, label: user.email }))
        }
        return []
    }, [users])

    const defaultValue = useMemo(() => {

        if (taskMembersOptions.length > 0) {
            const el = taskMembersOptions.find(obj => obj.label === task.assignee)

            return el
        }

        return undefined

    }, [taskMembersOptions])


    getTaskTypes(TASK_TYPE)
    getTaskCategory(TASK_CATEGORY)

    const handleInputChange = e => {
        const { name, value } = e.target;
        setTask({ ...task, [name]: value });
    };

    const handleTypeChange = e => {
        setSelectTaskType(e)
        setTask({ ...task, type: e.label });
    };

    const handleCategoryChange = e => {
        setSelectTaskCategory(e)
        setTask({ ...task, category: e.label });
    };

    const handleTaskAssigneeChange = e => {
        setSelectTaskAssignee(e)
        setTask({ ...task, assignee: e.label });
    };

    async function getCurrentProject() {
        const projectsRef = db.collection('projects');
        const queryRef = projectsRef.where('id', '==', id);
        const res = await queryRef.get();

        res.forEach(doc => {
            setCurrentProject(doc.data())
            setProjectKey(doc.id)
        });
    }


    const updateTask = () => {


        let newTasksList = [];

        let unchangedTasks = tasksList.filter(el => el.id !== task.id)


        unchangedTasks.map(el => newTasksList.push(el))

        newTasksList.push(task)



        db.collection("projects").doc(projectKey).update({ tasks: newTasksList }).then(() => {
            console.log("updated task!")
            handleEditData()
            handleEditModalToggle()
            toast('Task successfully edited!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                type: 'success',
                pauseOnHover: true
            });

        }).catch((err) => {
            toast('Error while updating task!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                type: 'error',
                pauseOnHover: true
            });
        })



    }




    useEffect(() => {
        getUsers()
        getCurrentProject()
    }, [])

    return (
        <form noValidate
            onSubmit={handleSubmit(updateTask)}
            className="">
            {error && <div className="mb-5 text-red-500">{error}</div>}


            <>
                <div className="flex flex-col max-w-md">


                    <FieldInput
                        name="name"
                        type="name"
                        labelText="Task name"
                        register={register}
                        errors={errors}
                        value={task.name}
                        onChange={handleInputChange}
                        defaultValue={task.name}
                    />

                    <FieldInput
                        name="description"
                        type="text"
                        labelText="Task description"
                        register={register}
                        errors={errors}
                        value={task.description}
                        onChange={handleInputChange}
                        defaultValue={task.description}
                        textarea
                    />

                    <Select
                        name="taskType"
                        type="text"
                        labelText="Task type"
                        options={taskTypeOptions}
                        register={register}
                        value={taskTypeOptions.find(obj => obj.value === selectTaskType)}
                        onChange={handleTypeChange}
                        defaultValue={taskTypeOptions.find(obj => obj.label === task.type)}
                        className="mb-10"
                    />

                    <Select
                        name="taskCategory"
                        type="text"
                        labelText="Task category"
                        options={taskCategoryOptions}
                        register={register}
                        value={taskCategoryOptions.find(obj => obj.value === selectTaskCategory)}
                        onChange={handleCategoryChange}
                        defaultValue={taskCategoryOptions.find(obj => obj.label === task.category)}
                        className="mb-10"
                    />

                    <Select
                        name="taskAssignee"
                        type="text"
                        labelText="Assign task to"
                        options={taskMembersOptions}
                        register={register}
                        value={defaultValue}
                        onChange={handleTaskAssigneeChange}
                        defaultValue={defaultValue}
                        className="mb-10"
                    />

                </div>


                <button className="bg-yellow-500 text-white py-2 font-bold inline-block mt-5 px-10 rounded-lg " type="submit">Save task</button>
            </>
        </form>
    );
};

export default EditTaskForm;
