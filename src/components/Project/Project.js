import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Project.module.css';
import { ReactComponent as ArrowRight } from '../../assets/icons/arrow-right.svg';

const Project = ({ id, name, category, pm, date }) => {
	return (
		<>
			<NavLink to={`project/${id}`} className={styles.Project}>
				<div className="w-1/6 px-4 py-2 text-center">{id}</div>
				<div className="w-1/6 px-4 py-2 text-center">{name}</div>
				<div className="w-1/6 px-4 py-2 text-center">{date}</div>
				<div className="w-1/6 px-4 py-2 text-center">{pm}</div>
				<div className="w-1/6 px-4 py-2 text-center">{category}</div>
				<div className="w-1/6 px-4 py-2 text-center flex items-center justify-center"><div className="block text-center bg-gray-200 hover:bg-gray-300 rounded p-1 w-5 mx-auto"><ArrowRight fill="#756f6f" /></div></div>
			</NavLink>
		</>
	);
};

export default Project;
