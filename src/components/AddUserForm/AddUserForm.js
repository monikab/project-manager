import React, { useContext, useState } from 'react';
import AuthContext, { getUser } from '../../contexts/AuthContext';
import Select from '../../components/Select/Select';
import FieldInput from '../../components/FieldInput/FieldInput';
import { useForm } from 'react-hook-form';
import 'firebase/auth';
import 'firebase/database';
import { USER_ROLES } from '../../constants';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import firebase from 'firebase/app';
import { db } from '../../firebase.config.js';
import randomColor from 'randomcolor';
import uuid from "uuid/v4";
import { toast } from "react-toastify";

const AddUserSchema = Yup.object().shape({
    email: Yup.string().email().required('Required field'),
    password: Yup.string().required('Required field')
});



const AddUserForm = ({ handleUserLoading, handleModalToggle }) => {

    const { register, handleSubmit, watch, errors, control } = useForm({
        resolver: yupResolver(AddUserSchema),
        mode: 'onChange',
    });

    const { signup } = useContext(AuthContext)
    const user = getUser();
    const [error, setError] = useState('');

    let rolesOptions = [];

    const getUserRoles = (roles) => {
        roles.map(type => {
            const { name, value } = type
            rolesOptions.push(type)
        })
    }


    getUserRoles(USER_ROLES)

    const userData = {
        id: '',
        email: '',
        password: '',
        role: '',
        color: ''
    }

    const [newUser, setNewUser] = useState(userData);
    const [userRole, setUserRole] = useState();
    

    const handleInputChange = e => {
        const { name, value } = e.target;
        setNewUser({ ...newUser, [name]: value });
    };

    const handleSelectChange = e => {
        setUserRole(e)
        setNewUser({ ...newUser, role: e.value });
    };

    const addNewUser = async () => {
       
        var data = {
            id: uuid(),
            email: newUser.email,
            password: newUser.password,
            role: newUser.role,
            color: randomColor({
                luminosity: 'bright',
                format: 'hex'
            })
        };

        try {
            setError("")
            await signup(data.email, data.password)
            await firebase.auth().updateCurrentUser(user)
            await db.collection("users").doc().set(data)
            handleUserLoading()
            handleModalToggle()
            toast('Added new user!', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				type: 'success',
				pauseOnHover: true
			});
        } catch (error) {
            setError(error.message)
        }
    };

    return (
        <form noValidate
            onSubmit={handleSubmit(addNewUser)}
            className="overflow-y-scroll">
            {error && <div className="mb-5 text-red-500">{error}</div>}

            <>
                <div className="flex flex-col max-w-md">

                    <FieldInput
                        name="email"
                        type="email"
                        labelText="User email"
                        register={register}
                        errors={errors}
                        value={newUser.email}
                        onChange={handleInputChange}
                        defaultValue=""
                    />
                    <FieldInput
                        name="password"
                        type="text"
                        labelText="User password"
                        register={register}
                        errors={errors}
                        value={newUser.password}
                        onChange={handleInputChange}
                        defaultValue=""
                    />

                    <Select
                        name="role"
                        type="text"
                        labelText="User role"
                        options={rolesOptions}
                        register={register}
                        value={rolesOptions.find(obj =>  obj.value === userRole )}
                        defaultValue={rolesOptions[0].value}
                        onChange={handleSelectChange}
                        className="mb-10"
                    />
                </div>


                <button className="bg-yellow-500 text-white py-2 font-bold inline-block mt-5 px-10 rounded-lg " type="submit">Add user</button>
            </>
        </form>
    );
};

export default AddUserForm;
