import React, { useState } from 'react';
import { Controller, Control } from 'react-hook-form';
import ReactSelect from 'react-select';
import chroma from 'chroma-js';

const customStyles = {
  container: (provided, state) => ({
    ...provided,
    display: 'flex',
    height: 'auto',
    minHeight: '40px',
    width: '100% !important',
    outline: 'none',
    borderBottom: state.isFocused ? '0' : '2px solid #C5C9CB',
  }),
  control: (provided, state) => ({
    ...provided,
    width: '100% !important',
    borderBottom: '2px',
    borderRadius: '0',
    borderColor: '',
    background: '#f7fafc',
    color: '#2D3133',
    paddingLeft: '10px',
    paddingRight: '10px',
    outline: 'none',
    border: state.isFocused ? '2px solid #2D3133' : '0',
    boxShadow: 'none',
    height: '100%',
    fontSize: '12px',
    ':active': {
      border: state.isFocused ? '2px solid #2D3133' : '0',
    },
    ':hover': {
      outline: 'none',
      border: state.isFocused ? '2px solid #2D3133' : '0',
      boxShadow: 'none',
    },
  }),
  option: (provided, state) => ({
    ...provided,
    opacity: state.isDisabled ? 0.5 : 1,
    pointerEvents: state.isDisabled ? 'none' : 'all',
    textDecoration: state.isDisabled ? 'line-through' : 'none',
  }),
  menu: (provided, state) => ({
    ...provided,
    background: '#F2F4F4',
    borderRadius: '0',
  }),

  multiValue: (styles, { data }) => {
    const color = chroma(data.color);

    return {
      ...styles,
      backgroundColor: color.alpha(0.1).css(),
      color: data.color

    };
  },
  multiValueLabel: (styles, { data }) => ({
    ...styles,

  }),

  // multiValue: (styles, { data }) => {
  //   const color = chroma(data.color);
  //   return {
  //     ...styles,
  //     backgroundColor: color.alpha(0.1).css(),
  //   };
  // },
  // multiValueLabel: (styles, { data }) => ({
  //   ...styles,
  //   color: data.color,
  // }),
  // multiValueRemove: (styles, { data }) => ({
  //   ...styles,
  //   color: data.color,
  //   ':hover': {
  //     backgroundColor: data.color,
  //     color: 'white',
  //   },
  // }),
  indicatorSeparator: (provided, state) => ({
    ...provided,
    display: 'none',
  }),
};

const Select = ({
  control,
  errors,
  name,
  options,
  className = '',
  showLabel = true,
  labelText = name,
  placeholder,
  register,
  defaultValue,
  data,
  ...rest
}) => {
  const [isFocused, setIsFocused] = useState(false);
  return (
    <div
      className={`${className} mb-5 outline-none field-select ${isFocused ? 'isFocused' : ''
        }`}
      onFocus={() => setIsFocused(true)}
      onBlur={() => setIsFocused(false)}
    >
      {showLabel && (
        <label className="block text-primary-title mb-2" htmlFor={name}>
          {labelText}
        </label>
      )}
      {control && (
        <Controller
          instanceId={name}
          as={<ReactSelect {...rest} />}
          name={name}
          options={options}
          control={control}
          defaultValue={defaultValue}
          errors={errors}
          styles={customStyles}
          ref={register}
        />
      )}
      {!control && (
        <ReactSelect
          {...rest}
          instanceId={name}
          name={name}
          options={options}
          control={control}
          defaultValue={defaultValue}
          errors={errors}
          styles={customStyles}
          className={` form-select`}
          placeholder={placeholder}
          ref={register}
          theme={theme => ({
            ...theme,
            colors: {
              ...theme.colors,
              primary25: '#28759a4f',
              primary: '#28759a',
            },
          })}
        />
      )}
      {errors && errors[name] && (
        <p className="text-sm text-teal-800">{errors[name].message}</p>
      )}
    </div>
  );
};
export default Select;
