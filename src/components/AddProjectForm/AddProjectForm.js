import React, { useState, useEffect, useMemo } from 'react';
import FieldInput from '../FieldInput/FieldInput';
import { useForm } from 'react-hook-form';
import { db } from '../../firebase.config.js';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import Select from '../Select/Select';
import { PROJECT_TYPE } from '../../constants';
import uuid from "uuid/v4";
import 'firebase/firestore'
import { toast } from "react-toastify";
import {getUser} from '../../contexts/AuthContext';

const ObjectDetailsSchema = Yup.object().shape({
	id: Yup.string().required('Obavezno polje'),
	name: Yup.string().required('Obavezno polje'),
	dateStarted: Yup.date().required('Obavezno polje').typeError('Datum mora biti u formatu: dd/mm/yy!'),
});

const AddProjectForm = ({handleModalClose, handleProjectsLoad}) => {

	const { register, handleSubmit, watch, errors, control } = useForm({
		resolver: yupResolver(ObjectDetailsSchema),
		mode: 'onChange',
	});

	const user = getUser()
	const [member, setMember] = useState([])
    
	
	async function loadMember() {

		db.collection("users")
			.get()
			.then(querySnapshot => {
				const data = querySnapshot.docs.map(doc => doc.data());

				let memberEmail = []
				memberEmail = data.find(el => el.email === user.email )
				setMember(memberEmail)

			});
	}
	
    
	const initialProjectState = {
		id: '',
		name: '',
		category: '',
		dateStarted: '',
		pm: '',
		tasks: [],
		members: []
	};
	const [project, setProject] = useState(initialProjectState);
    
	const [allProjects, setAllProjects] = useState();
	const [idError, setIdError] = useState(false);
	const [users, setUsers] = useState();

	let categorySelectOptions = [];

	const getProjectTypes = (project_types) => {
		project_types.map(type => {
			const { name, value } = type
			categorySelectOptions.push(type)
		})
	}
	getProjectTypes(PROJECT_TYPE)

	const [selectProjectType, setSelectProjectType] = useState(categorySelectOptions[0].value);
	const [selectProjectManager, setProjectManager] = useState();

	project.category = categorySelectOptions[0].label

	const handleInputChange = event => {
		const { name, value } = event.target;
		setProject({ ...project, [name]: value });
	};

	const handleCategoryChange = e => {
		setSelectProjectType(e)
		setProject({ ...project, category: e.label });
	};

	const handleMembersChange = e => {
		const membersList = [member.id]
		Object.keys(e).map(key => (membersList.push(e[key].value)))

		setProject({ ...project, members: membersList })

	}

	const handlePmChange = e => {
		setProjectManager(e)
		setProject({ ...project, pm: e.label })
	}

	async function getUsers() {
		const loadedUsers = await db.collection("users")
			.get()
			.then(querySnapshot => {
				const data = querySnapshot.docs.map(doc => doc.data());
				return data

			});
		setUsers(loadedUsers)
	}


	let selectPm = []

	const getPmUsers = () => {
		let projectManagers = [];
		projectManagers = (users && users.filter(el => el.role === 'pm'))
		projectManagers && projectManagers.map(el => selectPm.push({ label: el.email, value: el.id }))
	}

	getPmUsers()

	const saveProject = () => {
		var data = {
			id: project.id,
			name: project.name,
			category: project.category,
			pm: project.pm,
			dateStarted: project.dateStarted,
			tasks: [],
			members: project.members
		};

		db.collection("projects").doc().set(data).then(() => {
			toast('Project successfully created', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				type: 'success',
				pauseOnHover: true
			});

			
		}).catch((err) => {
			toast('Error while creating new project!', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				type: 'error',
				pauseOnHover: true
			});
		})

		handleModalClose()
		handleProjectsLoad()
	};


	async function getAllProjects() {
		await db.collection("projects")
			.get()
			.then(querySnapshot => {
				const data = querySnapshot.docs.map(doc => doc.data());

				setAllProjects(data)
			});
	}
	const checkIfIdExistis = async event => {
		const { name, value } = event.target;

		if (allProjects) {
			setIdError(Object.keys(allProjects).some(key => allProjects[key].id === event.target.value.trim()))
			setProject({ ...project, [name]: value })
		}

	};

	const membersSelectOptions = useMemo(() => {
		if (users) {
			let developers = users && users.filter(el => el.role !== 'pm' && el.role !== 'admin')
			return developers.map(user => ({ color: user.color, value: user.id, label: user.email }))
		}
		return []
	}, [users])

	const defaultValue = React.useMemo(() => {
		if (users && project) {
			return project.members
		}
		return null
	}, [users, project])

	useEffect(() => {
		getUsers()
		getAllProjects()
		loadMember()

	}, []);




	return (
		<form
			noValidate
			onSubmit={handleSubmit(saveProject)}
		>
			<div className="flex flex-col">
				{idError && <div>ID already exists!</div>}

				<FieldInput
					name="id"
					type="text"
					labelText="Project ID"
					register={register}
					errors={errors}
					value={project.id}
					onChange={checkIfIdExistis}
					defaultValue=""
				/>
				<FieldInput
					name="name"
					type="text"
					labelText="Project name"
					register={register}
					errors={errors}
					value={project.name}
					onChange={handleInputChange}
					defaultValue=""
				/>

				<Select
					name="pm"
					type="text"
					labelText="Project pm"
					register={register}
					errors={errors}

					options={selectPm}
					value={selectPm.find(obj => obj.value === selectProjectManager)}
					onChange={handlePmChange}
				// defaultValue={pmOptions[0].value}
				/>

				<Select
					name="category"
					type="text"
					labelText="Project category"
					options={categorySelectOptions}
					register={register}
					value={categorySelectOptions.find(obj => obj.value === selectProjectType)}
					onChange={handleCategoryChange}
					defaultValue={categorySelectOptions[0].value}
				/>

				<Select
					isMulti
					name="Members"
					options={membersSelectOptions}
					register={register}
					className="basic-multi-select"
					classNamePrefix="select"
					onChange={handleMembersChange}
					// value={getSelectedMembers()}
					defaultValue={defaultValue}
				/>

				<FieldInput
					name="dateStarted"
					type="date"
					labelText="Project date"
					register={register}
					errors={errors}
					value={project.dateStarted}
					onChange={handleInputChange}
				/>

				<button onClick={handleSubmit} className="bg-yellow-500 text-white py-2 font-bold inline-block px-10 rounded-lg">
					Submit
					</button>
			</div>
		</form>
	);
};

export default AddProjectForm;
