import React, { useEffect } from 'react';
import ModalPlugin from 'react-modal';
import styles from './Modal.module.css';


export default function Modal({
  children,
  isOpen,
  onRequestClose,
  headerTitle,
}) {
  useEffect(() => {
    if (typeof window !== 'undefined') {
      ModalPlugin.setAppElement('#root');
    }
  }, []);
  if (!isOpen) {
    return <></>;
  }
  return (
    <div>
      <ModalPlugin
        closeTimeoutMS={300}
        isOpen={isOpen}
        onRequestClose={onRequestClose}
        overlayClassName={` bg-gray-500 overflow-y-hidden bg-opacity-50 justify-center items-end md:items-center flex overflow-x-hidden fixed inset-0 z-50 outline-none m-auto`}
        className={`${styles.ModalWindow} relative mx-auto overflow-y-auto  md:rounded md:h-auto flex flex-col w-1/2  xl:w-1/3 bg-white outline-none`}
      >
       
        <div className="p-5 md:p-10 ">{children}</div>
      </ModalPlugin>
    </div>
  );
}
