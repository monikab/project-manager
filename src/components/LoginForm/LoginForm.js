import React, { useContext, useRef, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext from '../../contexts/AuthContext';
import 'firebase/auth';
import 'firebase/database';
import { db } from "../../firebase.config"

const LoginForm = () => {
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const { login } = useContext(AuthContext)
	const [error, setError] = useState('');
	const history = useHistory();

	async function handleSubmit(e) {
		e.preventDefault();

		try {
			if (email !== '' && password !== '') {
				let memberRole = []

				await db.collection("users")
					.get()
					.then(querySnapshot => {
						const data = querySnapshot.docs.map(doc => doc.data());
						Object.keys(data).find(el => data[el].email === email && memberRole.push(data[el].role))					
					});

				await login(email, password, memberRole[0]);
				history.push('/projects');
				return
			}

			throw new Error('Please enter email and password!')

		} catch (e) {
			setError("Wrong e-mail or password!");
		}
	}

	return (
		<form onSubmit={handleSubmit}>
			<h3>{error}</h3>
			<div className="flex flex-col max-w-md">

				<input
					name="email"
					type="email"
					placeholder="email"
					className="border-2 mb-5 p-2"
					onChange={e => setEmail(e.target.value)}
					value={email}
				/>

				<input
					name="password"
					type="password"
					placeholder="password"
					className="border-2 p-2"
					onChange={e => setPassword(e.target.value)}
					value={password}
				/>
			</div>

			<button className="bg-yellow-500 text-white py-2 font-bold inline-block mt-5 px-10 rounded-lg" type="submit">Login</button>

		</form>
	);
};

export default LoginForm;
