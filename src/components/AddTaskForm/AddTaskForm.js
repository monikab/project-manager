import React, { useContext, useRef, useState, useEffect, useMemo } from "react"
import FieldInput from '../FieldInput/FieldInput.js';
import { useForm } from 'react-hook-form';
import Select from '../Select/Select.js';
import Autocomplete from 'react-autocomplete';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useParams } from 'react-router-dom';
import { TASK_CATEGORY, TASK_TYPE } from '../../constants';
import { db } from '../../firebase.config.js';
import { getUserRole } from "../../contexts/AuthContext.js";
import uuid from "uuid/v4";
import 'firebase/database';
import 'firebase/firestore'
import firebase from 'firebase/app';
import { toast } from "react-toastify";
const AddTaskSchema = Yup.object().shape({
	name: Yup.string().required('Required field'),
	description: Yup.string()
});


const AddTaskForm = ({ handleRefreshData, closeHandler }) => {
	const { register, handleSubmit, watch, errors, control } = useForm({
		resolver: yupResolver(AddTaskSchema),
		mode: 'onChange',
	});

	const taskData = {
		id: uuid(),
		name: '',
		description: '',
		type: '',
		category: '',
		assignee: '',
		status: 'todo'
	};

	let { id } = useParams();
	const [error, setError] = useState('');
	const [newTask, setNewTask] = useState(taskData);
	const [users, setUsers] = useState();

	const [selectTaskType, setSelectTaskType] = useState();
	const [selectTaskCategory, setSelectTaskCategory] = useState();
	const [selectTaskAssignee, setSelectTaskAssignee] = useState();


	const [currentProject, setCurrentProject] = useState();
	const [projectKey, setProjectKey] = useState();

	async function getUsers() {
		const loadedUsers = await db.collection("users")
			.get()
			.then(querySnapshot => {
				const data = querySnapshot.docs.map(doc => doc.data());
				return data

			});
		setUsers(loadedUsers)
	}


	let taskTypeOptions = [];
	let taskCategoryOptions = [];


	const getTaskTypes = (task_types) => {
		task_types.map(type => {
			const { name, value } = type
			taskTypeOptions.push(type)
		})
	}

	const getTaskCategory = (task_categories) => {
		task_categories.map(type => {
			const { name, value } = type
			taskCategoryOptions.push(type)
		})
	}

	const taskMembersOptions = useMemo(() => {
		if (users) {
			let developers = users && users.filter(el => el.role !== 'pm')
			return developers.map(user => ({ color: user.color, value: user.id, label: user.email }))
		}
		return []
	}, [users])

	getTaskTypes(TASK_TYPE)
	getTaskCategory(TASK_CATEGORY)

	const handleInputChange = e => {
		const { name, value } = e.target;
		setNewTask({ ...newTask, [name]: value });
	};

	const handleTypeChange = e => {
		setSelectTaskType(e)
		setNewTask({ ...newTask, type: e.label });
	};

	const handleCategoryChange = e => {
		setSelectTaskCategory(e)
		setNewTask({ ...newTask, category: e.label });
	};

	const handleTaskAssigneeChange = e => {
		setSelectTaskAssignee(e)
		setNewTask({ ...newTask, assignee: e.label });
	};

	async function getCurrentProject() {
		const projectsRef = db.collection('projects');
		const queryRef = projectsRef.where('id', '==', id);
		const res = await queryRef.get();

		res.forEach(doc => {
			setCurrentProject(doc.data())
			setProjectKey(doc.id)
		});
	}

	const addNewTask = async () => {
		try {
			await db.collection("projects").doc(projectKey).update({ tasks: firebase.firestore.FieldValue.arrayUnion(newTask) }).then(() => {
				console.log("added new task!")
			})
			handleRefreshData()
			closeHandler(false)
			toast('Task successfully created', {
				position: "top-right",
				autoClose: 2000,
				hideProgressBar: false,
				closeOnClick: true,
				type: 'success',
				pauseOnHover: true
			});


		}
		catch (err) {
			throw new Error(err.message)
		}
	}


	useEffect(() => {
		getUsers()
		getCurrentProject()
	}, [])

	return (
		<form noValidate
			onSubmit={handleSubmit(addNewTask)}
			className="overflow-y-scroll">
			{error && <div className="mb-5 text-red-500">{error}</div>}


			<>
				<div className="flex flex-col max-w-md">

					<FieldInput
						name="name"
						type="name"
						labelText="Task name"
						register={register}
						errors={errors}
						value={newTask.name}
						onChange={handleInputChange}
						defaultValue=""
					/>

					<FieldInput
						name="description"
						type="text"
						labelText="Task description"
						register={register}
						errors={errors}
						value={newTask.description}
						onChange={handleInputChange}
						defaultValue=""
						textarea
					/>

					<Select
						name="taskType"
						type="text"
						labelText="Task type"
						options={taskTypeOptions}
						register={register}
						value={taskTypeOptions.find(obj => obj.value === selectTaskType)}
						onChange={handleTypeChange}
						defaultValue={taskTypeOptions[0].value}
						className="mb-10"
					/>

					<Select
						name="taskCategory"
						type="text"
						labelText="Task category"
						options={taskCategoryOptions}
						register={register}
						value={taskCategoryOptions.find(obj => obj.value === selectTaskCategory)}
						onChange={handleCategoryChange}
						defaultValue={taskCategoryOptions[0].value}
						className="mb-10"
					/>

					<Select
						name="taskAssignee"
						type="text"
						labelText="Assign task to"
						options={taskMembersOptions}
						register={register}
						value={taskMembersOptions.find(obj => obj.value === selectTaskAssignee)}
						onChange={handleTaskAssigneeChange}
						//defaultValue={taskMembersOptions[0].value}
						className="mb-10"
					/>

				</div>


				<button className="bg-yellow-500 text-white py-2 font-bold inline-block mt-5 px-10 rounded-lg " type="submit">Add task</button>
			</>
		</form>
	);
};

export default AddTaskForm;
