import firebase from "firebase/app"
import "firebase/auth"
import 'firebase/firestore'

const app = firebase.initializeApp({
  apiKey: "AIzaSyD8yCdDm--NYWF_H3DaHYjQcweNFBGdUA0",
  authDomain: "project-manager-55175.firebaseapp.com",
  databaseURL: "https://project-manager-55175.firebaseio.com",
  projectId: "project-manager-55175",
  storageBucket: "project-manager-55175.appspot.com",
  messagingSenderId: "1044292859071",
  appId: "1:1044292859071:web:c15b18ad313d0205f8420a",
  measurementId: "G-CYXXE371DR"
});

const db = app.firestore();



export { app, db }