import React, { useContext, useState, useEffect, createContext } from "react"
import { app, db } from "../firebase.config"
import 'firebase/database';

const AuthContext = createContext()
export default AuthContext;



export const getUser = () => {
  const user = app.auth().currentUser;
  return user || undefined;
};

export const getUserRole = () => {
  const role = localStorage.getItem('userRole');
  return role || undefined;
}

export const hasPermission = (roleWithAccess) => {
  const userRole = getUserRole();
  if (userRole === roleWithAccess) {
    return true
  }
}

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState()
  const [loading, setLoading] = useState(true)
  const auth = app.auth()
  
  function signup(email, password) {
    return auth.createUserWithEmailAndPassword(email, password)
  }

  function login(email, password, role) {
    localStorage.setItem('userRole', role);
    return auth.signInWithEmailAndPassword(email, password)
  }

  function logout() {
    return auth.signOut()
  }


  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(user => {
      setCurrentUser(user)

      setLoading(false)
    })

    return unsubscribe
  }, [])

  const value = {
    currentUser,
    login,
    signup,
    logout
  }

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
