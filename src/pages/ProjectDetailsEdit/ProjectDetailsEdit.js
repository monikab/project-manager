import React, { useState, useEffect, useMemo } from 'react';
import { useParams, NavLink } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import FieldInput from '../../components/FieldInput/FieldInput';
import Select from '../../components/Select/Select';
import { PROJECT_TYPE } from '../../constants';
import { db } from '../../firebase.config.js';
import { toast } from "react-toastify";

const ProjectDetailsEdit = () => {
	let { id } = useParams();
	const [projectName, setProjectName] = useState();
	const [projectKey, setProjectKey] = useState();
	const [users, setUsers] = useState();

	const history = useHistory();

	const initialFormState = {
		name: '',
		category: '',
		dateStarted: '',
		pm: '',
		tasks: []
	};

	let options = [];
	
	const getProjectTypes = (project_types) => {
		project_types.map(type => {
			options.push(type)
		})
	}

	getProjectTypes(PROJECT_TYPE)

	const [currentProject, setCurrentProject] = useState(initialFormState);


	async function getCurrentProject() {
		const projectsRef = db.collection('projects');
		const queryRef = projectsRef.where('id', '==', id);
		const res = await queryRef.get();

		res.forEach(doc => {
			setCurrentProject(doc.data())
			setProjectKey(doc.id)
			setProjectName(doc.data().name)
		});
	}

	async function getUsers() {
		const loadedUsers = await db.collection("users")
			.get()
			.then(querySnapshot => {
				const data = querySnapshot.docs.map(doc => doc.data());
				return data

			});

		setUsers(loadedUsers)
	}

	const handleInputChange = event => {
		const { name, value } = event.target;
		setCurrentProject({ ...currentProject, [name]: value });
	}

	const handleCategoryChange = e => {
		setCurrentProject({ ...currentProject, category: e.label });
	};

	const handleMembersChange = e => {
		const membersList = []
		e && Object.keys(e).map(key => (membersList.push(e[key].value)))
		setCurrentProject({ ...currentProject, members: membersList })
	}

	const handleProjectEdit = () => {
		const projectsRef = db.collection('projects');
		projectsRef.doc(projectKey).update(currentProject).then(res => {
			history.push(`/project/${currentProject.id}`);
			toast('Project successfully edited', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				type: 'success',
				pauseOnHover: true
			});
		})
	}

	const membersSelectOptions = useMemo(() => {
		if (users) {
			let developers = users && users.filter(el => el.role !== 'pm' && el.role !== 'admin')
			return developers.map(user => ({  color: user.color , value: user.id, label: user.email}))
		}
		return []
	}, [users])

	const membersDefaultValue = useMemo(() => {
		if (currentProject.members && users) {
			return currentProject.members.map(member => {
				const selectedMember = users.find(user => user.id === member)
				if (selectedMember) {
					return {label: selectedMember.email, value: selectedMember.id, color: selectedMember.color}
				}
			})
		}
		return []
	}, [currentProject, users])

	useEffect(() => {
		getUsers()
		getCurrentProject()
	}, []);


	return (
		<>
			{
				currentProject && (
					<div key={currentProject.id}>

						<h1 className="text-2xl text-primary-title">Edit of: {projectName}</h1>

						<form className="flex pt-10 text-primary-title flex-col max-w-md">
							<FieldInput
								name="id"
								type="text"
								labelText="Project ID"
								value={currentProject.id}
								onChange={handleInputChange}
								disabled
								className="opacity-50"
							/>
							<FieldInput
								name="name"
								type="text"
								labelText="Project name"
								value={currentProject.name}
								onChange={handleInputChange}
							/>
							<FieldInput
								name="pm"
								type="text"
								labelText="Project pm"
								value={currentProject.pm}
								onChange={handleInputChange}
								disabled
							/>
							<Select
								name="category"
								type="text"
								labelText="Project category"
								options={options}
								value={options.find(obj => obj.label === currentProject.category)}
								onChange={handleCategoryChange}
							/>

							<Select
								isMulti
								name="Members"
								options={membersSelectOptions}
								className="basic-multi-select"
								classNamePrefix="select"
								onChange={handleMembersChange}
								defaultValue={membersDefaultValue}
								data={membersSelectOptions}
							/>

							<FieldInput
								name="dateStarted"
								type="date"
								labelText="Project date"
								value={currentProject.dateStarted}
								onChange={handleInputChange}
							/>
						</form>
						<NavLink to={`/project/${id}`} className="p-2 mr-4">
							Cancel
						</NavLink>
						<button
							onClick={handleProjectEdit}
							className="bg-yellow-500 text-white py-2 font-bold inline-block mt-5 px-10 rounded-lg"
						>
							Save changes
						</button>
					</div>
				)}
		</>
	);
};

export default ProjectDetailsEdit;
