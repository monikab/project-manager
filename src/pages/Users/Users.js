import React, { useState, useEffect } from 'react';
import Modal from '../../components/Modal/Modal';
import { ReactComponent as CloseIcon } from '../../assets/icons/close-icon.svg';
import AddUserForm from '../../components/AddUserForm/AddUserForm';
import { hasPermission } from '../../contexts/AuthContext';
import { db } from "../../firebase.config"

const Users = () => {

	const [isAddUserModalOpened, setIsAddUserModalOpened] = useState(false);

	const toggleUserModal = () => {
		setIsAddUserModalOpened(!isAddUserModalOpened);
	};

	const [users, setUsers] = useState();

	const loadUsers = () => {
		db.collection("users")
			.get()
			.then(querySnapshot => {
				const data = querySnapshot.docs.map(doc => doc.data());
				setUsers(data)
			})
	};

	useEffect(() => {
		loadUsers();
	}, [isAddUserModalOpened]);



	return (
		<div>

			{(hasPermission("pm") || hasPermission("admin")) && <button
				className="right-0 absolute bg-yellow-500 text-white py-2 font-bold px-10 rounded-lg"
				onClick={toggleUserModal}
			>
				+ Add user
			</button>}


			<h1 className="text-primary-800 text-3xl font-semibold pb-10">Users</h1>

			{users ? <ul className="my-5">
				{hasPermission("admin") && <>
					<span className="border-b-2 w-2/3 text-xl text-primary-800 block pb-2 mb-2">Administrators</span>
					{users && Object.keys(users).map(key => users[key].role === 'admin' && <li key={key}>{users[key].email}</li>)}
				</>}
				<span className="border-b-2 w-2/3 text-xl text-primary-800 mt-8 block pb-2 mb-2">Project Managers</span>
				{users && Object.keys(users).map(key => users[key].role === 'pm' && <li key={key}>{users[key].email}</li>)}
				<span className="border-b-2 w-2/3 text-xl text-primary-800 mt-8 block pb-2 mb-2">Developers</span>
				{users && Object.keys(users).map(key => users[key].role === 'dev' && <li key={key}>{users[key].email}</li>)}
			</ul> : "No users!"}




			<Modal
				isOpen={isAddUserModalOpened}
				onRequestClose={toggleUserModal}
				headerTitle="Add new task"
			>
				<div className="flex justify-between">
					<h1 className="mb-10  text-xl">Add new user</h1>
					<a onClick={toggleUserModal} className="h-5 w-5 cursor-pointer">
						<CloseIcon
							fill="gray"
							className="absolute top-50px right-40 md:right-70 "
						></CloseIcon>
					</a>
				</div>
				<AddUserForm handleModalToggle={toggleUserModal} handleUserLoading={loadUsers} />
			</Modal>
		</div>
	);
};

export default Users;
