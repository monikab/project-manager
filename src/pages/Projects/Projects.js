import React, { useState, useEffect } from 'react';
import Project from '../../components/Project/Project';
import AddProjectForm from '../../components/AddProjectForm/AddProjectForm';
import Modal from '../../components/Modal/Modal';
import { ReactComponent as CloseIcon } from '../../assets/icons/close-icon.svg';
import { hasPermission, getUserRole } from '../../contexts/AuthContext';
import { db } from '../../firebase.config.js';
import { getUser } from '../../contexts/AuthContext';
import styles from './Projects.module.css';

const Projects = () => {
	const [projects, setProjects] = useState([]);
	const [isAddProjectModalOpened, setIsAddProjectModalOpened] = useState(false);
	const userEmail = getUser().email;

	const toggleAddProjectModal = () => {
		setIsAddProjectModalOpened(!isAddProjectModalOpened);
	};

	const userRole = getUserRole()

	async function loadProjectsForUser() {

		const usersRef = db.collection('users');
		const queryRef = usersRef.where('email', '==', userEmail);
		const res = await queryRef.get();

		let userId
		let pmEmail

		res.forEach(doc => {
			userId = doc.data().id
			pmEmail = doc.data().email
		});

		if (userId) {
			let projekti = []
			const projectsRef = db.collection('projects');
			const projectPmRef = db.collection('projects');

			if (userRole != 'pm') {
				const projectQueryRef = projectsRef.where('members', 'array-contains', userId)
				const projRes = await projectQueryRef.get();

				projRes.forEach(el => projekti.push(el.data()))
			}
			else {
				const projectQueryRefPm = projectPmRef.where('pm', '==', pmEmail)
				const projPmRes = await projectQueryRefPm.get();

				projPmRes.forEach(el => projekti.push(el.data()))
			}

			setProjects(projekti)
		}
	};


	useEffect(() => {
		loadProjectsForUser();
	}, []);

	return (
		<>
			<div>
				<h1 className="text-primary-800 text-3xl font-semibold pb-10">
					Projects
				</h1>

				{(hasPermission("pm") || hasPermission("admin")) && <button
					className="right-0 absolute bg-yellow-500 text-white py-2 font-bold px-10 rounded-lg top-0"
					onClick={toggleAddProjectModal}
				>
					+ Add new project
				</button>}

				{projects &&
					projects.length > 0 ?
					<div className={styles.ProjectsWrapper}>
						<div className="flex p-3">
							<div className="w-1/6 px-4 py-2 text-xs text-center">ID</div>
							<div className="w-1/6 px-4 py-2 text-xs text-center">Name</div>
							<div className="w-1/6 px-4 py-2 text-xs text-center">Date started</div>
							<div className="w-1/6 px-4 py-2 text-xs text-center">PM</div>
							<div className="w-1/6 px-4 py-2 text-xs text-center">Category</div>
							<div className="w-1/6 px-4 py-2 text-xs text-center"></div>

						</div>
						<div>
							{projects.map(project => (
								<Project
									key={project.id}
									name={project.name}
									id={project.id}
									pm={project.pm}
									date={project.dateStarted}
									category={project.category}
								/>

							))}
						</div>
					</div> : "No projects to show!"

				}
			</div>

			<Modal
				isOpen={isAddProjectModalOpened}
				onRequestClose={toggleAddProjectModal}
				headerTitle="Add new task"
			>
				<div className="flex justify-between">
					<h1 className="mb-10  text-xl">Add new project</h1>
					<a onClick={toggleAddProjectModal} className="h-5 w-5 cursor-pointer">
						<CloseIcon
							fill="gray"
							className="absolute top-50px right-40 md:right-70 "
						></CloseIcon>
					</a>
				</div>
				<AddProjectForm handleModalClose={toggleAddProjectModal} handleProjectsLoad={loadProjectsForUser} />
			</Modal>
		</>
	);
};

export default Projects;
