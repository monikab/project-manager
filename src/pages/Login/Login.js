import React, { useState, useContext } from 'react';
import LoginForm from '../../components/LoginForm/LoginForm';

const Login = () => {

	return (
		<div>
			<h1 className="text-xl font-bold pb-8">Login</h1>
			<LoginForm />
		</div>
	);
};

export default Login;
