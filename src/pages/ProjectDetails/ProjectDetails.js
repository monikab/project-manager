import React, { useState, useEffect, useMemo } from 'react';
import { useParams, NavLink } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { hasPermission } from '../../contexts/AuthContext';
import { db } from '../../firebase.config.js';
import Modal from '../../components/Modal/Modal';
import { ReactComponent as CloseIcon } from '../../assets/icons/close-icon.svg';
import { ReactComponent as ArrowLeft } from '../../assets/icons/arrow-left.svg';

import { toast } from "react-toastify";

const ProjectDetails = () => {
	let { id } = useParams();
	const [project, setProject] = useState();
	const [members, setMembers] = useState([])
	const history = useHistory();

	const [isDeleteModalOpened, setIsDeleteModalOpened] = useState();


	const toggleDeleteModal = () => {
		setIsDeleteModalOpened(!isDeleteModalOpened)
	}

	const handleProjectDelete = async () => {
		const projectsRef = db.collection('projects');
		const queryRef = projectsRef.where('id', '==', id);
		const res = await queryRef.get();


		res.forEach(doc => {
			doc.ref.delete();
			history.push("/projects")
			toast('Project succesfully deleted!', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				type: 'success',
				pauseOnHover: true
			});
		});

	};

	async function getProject() {
		const projectsRef = db.collection('projects');
		const queryRef = projectsRef.where('id', '==', id);
		const res = await queryRef.get();

		res.forEach(doc => {
			setProject(doc.data())
		});
	}

	async function loadMembers() {

		db.collection("users")
			.get()
			.then(querySnapshot => {
				const data = querySnapshot.docs.map(doc => doc.data());

				let membersEmails = []
				project.members.filter(key => Object.keys(data).find(el => data[el].id === key && membersEmails.push(data[el].email)))

				setMembers(membersEmails)

			});
	}

	const getMembers = async () => {
		loadMembers()

	}

	useEffect(() => {
		getProject()
	}, []);

	useEffect(() => {
		if (project) {
			getMembers()
		}
	}, [project]);



	return (
		<>
			{project &&
				<div key={id}>
					
					<h1 className="text-2xl text-primary-title flex items-center"><NavLink to="/projects" className="w-8 mr-3 block"><ArrowLeft fill="#004753"/></NavLink>{project.name}</h1>

					<div className="flex justify-end">

						{
							(hasPermission("admin") || hasPermission("pm")) && <>
								<NavLink
									className="btn bg-gray-200 px-5 py-1 rounded text-center"
									to={`/project/${id}/edit`}
								>
									Edit
								</NavLink>

								<button
									type="submit"
									className="btn bg-red-500 ml-4 px-5 py-1 rounded text-center text-white"
									onClick={toggleDeleteModal}
								>
									Delete
								</button>
							</>
						}


					</div>
					<form className="flex pt-10 text-primary-title">
						<div className="w-1/2">
							<p className="py-2">Project ID: {project.id}</p>

							<p className="py-2">Project manager: {project.pm}</p>

							<p className="py-2">Category: {project.category}</p>

							<p className="py-2">Date started: {project.dateStarted}</p>

						</div>

						<div className="w-1/2">

							<div className="py-2">Members: {members && members.map(el => <li key={el}>{el}</li>)}</div>
							<p className="py-2">
								Total tasks: {project.tasks && project.tasks.length}
							</p>

							{/* <p className="py-2">To-do tasks: {project.tasks && project.tasks.map(el => el.status === 'todo' )}</p> */}

						</div>
					</form>

					<NavLink
						to={`/project/${id}/kanban`}
						className="bg-yellow-500 text-white py-2 font-bold inline-block mt-5 px-10 rounded-lg "
					>
						Kanban Board
						</NavLink>
				</div>

			}

			<Modal
				isOpen={isDeleteModalOpened}
				onRequestClose={toggleDeleteModal}
			>
				<div className="flex justify-between">
					<h1 className="mb-10 text-xl">Are you sure you want to delete this project?</h1>
					<a onClick={toggleDeleteModal} className="h-5 w-5 cursor-pointer">
						<CloseIcon
							fill="gray"
							className="absolute top-50px right-40 md:right-70 "
						></CloseIcon>
					</a>
				</div>
				<div className="flex justify-center items-center">
					<button className="px-2 py-1 bg-red-500 text-white rounded mr-10" onClick={handleProjectDelete}>Yes</button>
					<a className="p-2 cursor-pointer" onClick={toggleDeleteModal}>No</a>
				</div>
			</Modal>
		</>
	);
};

export default ProjectDetails;
