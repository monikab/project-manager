import React, { useEffect, useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { useParams } from 'react-router-dom';
import { db } from '../../firebase.config.js';
import 'firebase/firestore'
import Modal from '../../components/Modal/Modal.js';
import EditTaskForm from '../../components/EditTaskForm/EditTaskForm.js';
import { ReactComponent as CloseIcon } from '../../assets/icons/close-icon.svg';
import { ReactComponent as TrashIcon } from '../../assets/icons/trash-icon.svg';

const KanbanBoard = ({ data, handleRefreshData }) => {

    let { id } = useParams();
    const [columns, setColumns] = useState();
    const [isDeleteTaskModalOpened, setIsDeleteTaskModalOpened] = useState();
    const [tasks, setTasks] = useState();
    const [projectKey, setProjectKey] = useState();


    async function getCurrentProject() {
        const projectsRef = db.collection('projects');
        const queryRef = projectsRef.where('id', '==', id);
        const res = await queryRef.get();

        res.forEach(doc => {
            setProjectKey(doc.id)
            setTasks(doc.data().tasks)
        });
    }


    const onDragEnd = (result, columns, setColumns) => {
        if (!result.destination) return;
        const { source, destination } = result;

        if (source.droppableId !== destination.droppableId) {
            const sourceColumn = columns[source.droppableId];
            const destColumn = columns[destination.droppableId];
            const sourceItems = [...sourceColumn.items];
            const destItems = [...destColumn.items];
            const [removed] = sourceItems.splice(source.index, 1);
            destItems.splice(destination.index, 0, removed);

            setColumns({
                ...columns,
                [source.droppableId]: {
                    ...sourceColumn,
                    items: sourceItems
                },
                [destination.droppableId]: {
                    ...destColumn,
                    items: destItems
                }
            });

            destItems.forEach(item => {

                let updatedTasks = []
                let otherTasks = tasks.filter(task => task.id !== item.id)

                otherTasks.map(el => updatedTasks.push(el))

                const povuceniTask = tasks.filter(task => task.id === item.id)

                povuceniTask[0].status = destColumn.id

                updatedTasks.push(povuceniTask[0])

                db.collection("projects").doc(projectKey).update({ tasks: updatedTasks }).then(() => {
                    console.log("tasks updated")
                }).catch((err) => { console.log(err) })

            })
        } else {
            const column = columns[source.droppableId];
            const copiedItems = [...column.items];
            const [removed] = copiedItems.splice(source.index, 1);
            copiedItems.splice(destination.index, 0, removed);
            setColumns({
                ...columns,
                [source.droppableId]: {
                    ...column,
                    items: copiedItems
                }
            });
        }

    };

    const [selectedTask, setSelectedTask] = useState();
    const [isTaskModalOpened, setIsTaskModalOpened] = useState(false);

    const handleTaskModal = (task) => {
        setSelectedTask(task)
        toggleTaskModal()
    }

    const toggleTaskModal = () => {
        setIsTaskModalOpened(!isTaskModalOpened);
    };

    const toggleDeleteTaskModal = (task) => {
        setIsDeleteTaskModalOpened(!isDeleteTaskModalOpened);
        setSelectedTask(task)
    }

    const handleTaskDelete = async () => {

        let taskId = selectedTask && selectedTask.id

        const projectRef = db.collection('projects').doc(projectKey);
        let project = await projectRef.get();
        let projectTasks = project.data().tasks
        let tasksWithoutDeletedOne = projectTasks.filter(el => el.id !== taskId)

        try {

            await projectRef.update({ tasks: tasksWithoutDeletedOne }).then(() => {
                console.log("tasks updated")

            })

            handleRefreshData()
            toggleDeleteTaskModal()
        }
        catch {
            console.log("error")
        }

    };

    useEffect(() => {
        if (data) {
            setColumns(data)
            getCurrentProject()

        }
    }, [data])

    return (
        <div style={{ display: "flex", height: "100%", margin: "40px 0" }}>
            { columns && <DragDropContext
                onDragEnd={result => onDragEnd(result, columns, setColumns)}
            >
                {Object.entries(columns).map(([columnId, column], index) => {
                    return (
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                color: "#046F83",
                            }}
                            key={columnId}
                        >
                            <h2>{column.name}</h2>
                            <div style={{ margin: 8 }}>
                                <Droppable droppableId={columnId} key={columnId}>
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                {...provided.droppableProps}
                                                ref={provided.innerRef}
                                                style={{
                                                    background: snapshot.isDraggingOver
                                                        ? "#F5F5F5"
                                                        : "#F5F5F5",
                                                    padding: 4,
                                                    width: 200,
                                                    minHeight: '100vh',
                                                    borderRadius: 4
                                                }}
                                            >
                                                { column.items && column.items.map((item, index) => {
                                                    return (
                                                        <>
                                                            <Draggable
                                                                key={item.id}
                                                                draggableId={item.id}
                                                                index={index}
                                                            >
                                                                {(provided, snapshot) => {
                                                                    return (
                                                                        <div
                                                                            ref={provided.innerRef}
                                                                            {...provided.draggableProps}
                                                                            {...provided.dragHandleProps}
                                                                            style={{
                                                                                userSelect: "none",
                                                                                padding: 15,
                                                                                margin: "0 0 8px 0",
                                                                                minHeight: "50px",
                                                                                backgroundColor: snapshot.isDragging
                                                                                    ? "#B3CACC"
                                                                                    : "#ECF6F7",
                                                                                color: "#046F83",
                                                                                boxShadow: "0px 2px 4px #00000020",
                                                                                borderRadius: "4px",
                                                                                position: "relative",
                                                                                overflowX: "hidden",
                                                                                ...provided.draggableProps.style
                                                                            }}
                                                                        >
                                                                            <TrashIcon onClick={() => toggleDeleteTaskModal(item)} className="w-4 h-4 absolute right-4 top-4 opacity-25 hover:opacity-100 cursor-pointer" />
                                                                            <a className="text-left hover:underline cursor-pointer block pr-5" onClick={() => handleTaskModal(item)}>{item.name}</a>
                                                                            {item.category === 'Frontend' &&
                                                                                <div className="mt-2 text-xs justify-center items-center flex bg-green-400 text-white w-5 text-center rounded-sm h-5">
                                                                                    FE
                                                                                </div>
                                                                            }

                                                                            {item.category === 'Backend' &&
                                                                                <div className="mt-2 text-xs justify-center items-center flex bg-orange-400 text-white w-5 text-center rounded-sm h-5">
                                                                                    BE
                                                                            </div>
                                                                            }

                                                                            {item.category === 'Project Management' &&
                                                                                <div className="mt-2 text-xs justify-center items-center flex bg-purple-400 text-white w-5 text-center rounded-sm h-5">
                                                                                    PM
                                                                            </div>
                                                                            }
                                                                        </div>
                                                                    );
                                                                }}
                                                            </Draggable>

                                                        </>
                                                    );
                                                })}
                                                {provided.placeholder}
                                            </div>
                                        );
                                    }}
                                </Droppable>

                            </div>
                        </div>
                    );
                })}
            </DragDropContext>
            }
            <Modal
                isOpen={isTaskModalOpened}
                onRequestClose={toggleTaskModal}
                headerTitle={selectedTask && selectedTask.name}
            >
                <div className="flex justify-between">
                    <div className="mb-10">
                        <h2 className="text-2xl text-primary-title">{selectedTask && selectedTask.name}</h2>
                        <span className="text-xs">ID: {selectedTask && selectedTask.id}</span>
                    </div>
                    <a onClick={toggleTaskModal} className="h-5 w-5 cursor-pointer">
                        <CloseIcon
                            fill="gray"
                            className="absolute top-50px right-40 md:right-70 "
                        ></CloseIcon>
                    </a>
                </div>
                <EditTaskForm data={selectedTask} tasksList={tasks} handleEditData={handleRefreshData} handleEditModalToggle={toggleTaskModal}/>
            </Modal>

            <Modal
                isOpen={isDeleteTaskModalOpened}
                onRequestClose={toggleDeleteTaskModal}
            >
                <div className="flex justify-between">
                    <h1 className="mb-10 text-xl">Are you sure you want to delete this task?</h1>
                    <a onClick={toggleDeleteTaskModal} className="h-5 w-5 cursor-pointer">
                        <CloseIcon
                            fill="gray"
                            className="absolute top-50px right-40 md:right-70"
                        ></CloseIcon>
                    </a>
                </div>
                <div className="flex justify-center items-center">
                    <button className="px-2 py-1 bg-red-500 text-white rounded mr-10" onClick={handleTaskDelete}>Yes</button>
                    <a className="p-2 cursor-pointer" onClick={toggleDeleteTaskModal}>No</a>
                </div>
            </Modal>
        </div>

    );
}

export default KanbanBoard;
