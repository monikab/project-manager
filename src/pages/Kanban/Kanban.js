import React, { useState, useEffect, useMemo } from 'react';
import { useParams, NavLink } from 'react-router-dom';
import Modal from '../../components/Modal/Modal.js';
import AddTaskForm from '../../components/AddTaskForm/AddTaskForm.js';
import { ReactComponent as CloseIcon } from '../../assets/icons/close-icon.svg';
import KanbanBoard from './KanbanBoard';
import { db } from '../../firebase.config.js';
import uuid from "uuid/v4";
import { ReactComponent as ArrowLeft } from '../../assets/icons/arrow-left.svg';


const Kanban = () => {
	let { id } = useParams();
	const [isAddTaskModalOpened, setIsAddTaskModalOpened] = useState(false);

	const toggleAddTaskModal = () => {
		setIsAddTaskModalOpened(!isAddTaskModalOpened);
	};

	const [project, setProject] = useState([]);
	const [tasksList, setTasksList] = useState();
	const [projectKey, setProjectKey] = useState();


	async function getCurrentProject() {
		const projectsRef = db.collection('projects');
		const queryRef = projectsRef.where('id', '==', id);
		const res = await queryRef.get();

		res.forEach(doc => {
			setProject(doc.data())
			setProjectKey(doc.id)
			setTasksList(doc.data().tasks)
		});
	}

	const getColumns = useMemo(() => {
		if (tasksList) {
			return {
				[uuid()]: {
					name: "To do",
					id: "todo",
					items: tasksList && tasksList.filter(el => el.status === 'todo')
				},
				[uuid()]: {
					name: "In Progress",
					id: "wip",
					items: tasksList && tasksList.filter(el => el.status === 'wip')
				},
				[uuid()]: {
					name: "Testing",
					id: "testing",
					items: tasksList && tasksList.filter(el => el.status === 'testing')
				},
				[uuid()]: {
					name: "Done",
					id: "done",
					items: tasksList && tasksList.filter(el => el.status === 'done')
				}
			};
		}
	}, [
		tasksList
	])


	useEffect(() => {
		getCurrentProject()
	}, []);


	return (
		<div>
			<div className="flex flex-col">
				<h1 className="text-2xl text-primary-title flex">
					<NavLink to={`/project/${id}`} className="w-8 mr-3 block"><ArrowLeft fill="#004753"/></NavLink>
					Kanban board - {project && project.name}
				</h1>
				<button className="right-0 absolute bg-yellow-500 text-white py-2 font-bold px-10 rounded-lg" onClick={toggleAddTaskModal}>
					+ Add new task
				</button>
			</div>



			<Modal
				isOpen={isAddTaskModalOpened}
				onRequestClose={toggleAddTaskModal}
				headerTitle="Add new task"
				data={project}
			>
				<div className="flex justify-between">
					<h1 className="mb-10  text-xl">Add new task</h1>
					<a onClick={toggleAddTaskModal} className="h-5 w-5 cursor-pointer">
						<CloseIcon
							fill="gray"
							className="absolute top-50px right-40 md:right-70 "
						></CloseIcon>
					</a>
				</div>
				<AddTaskForm handleRefreshData={getCurrentProject} closeHandler={setIsAddTaskModalOpened} />
			</Modal>
			<KanbanBoard data={getColumns} handleRefreshData={getCurrentProject} />
		</div>
	);
};

export default Kanban;
