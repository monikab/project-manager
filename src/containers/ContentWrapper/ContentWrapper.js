import React from 'react';
import styles from './ContentWrapper.module.css';

const Container = ({children}) => {
	return <div className={`${styles.ContentWrapper} `}>{children}</div>;
};

export default Container;
