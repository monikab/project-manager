import React from 'react';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import Container from '../ContentWrapper/ContentWrapper';

const Layout = ({children}) => {
	return (
		<>
			<Toolbar />

			<Container>
				<main className="relative">{children}</main>
			</Container>
		</>
	);
};

export default Layout;
